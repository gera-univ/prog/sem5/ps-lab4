#include <bits/stdc++.h>

using namespace std;
using namespace chrono;

class Integral
{
	double a, b;
	int N;
	function<double(double)> f;
	string name;
public:
	Integral(function<double(double)> f, double a, double b, int N, string name)
		: f(f), a(a), b(b), N(N), name(name) {}
	
	double value()
	{
		double sum = 0;
		double h = (b - a)/N;
		for (int i = 0; i <= N - 1; ++i) {
			sum += abs(f(a + i * h) * h);
		}
		return sum;
	}

	double value_parallel(int n_threads)
	{
		double sum = 0;
		double h = (b - a)/N;
		int parts = N / n_threads;
		vector<future<double>> futures;
		for (int i = 0; i < n_threads; ++i) {
			futures.push_back(async(launch::async, [=] () {
			double sum = 0;
			for (int j = parts*i; j < parts*(i+1); ++j) {
				sum += abs(f(a + j * h) * h);
			}
			return sum;
			}));
		}
		for (int i = 0; i < n_threads; ++i) {
			sum += futures[i].get();
		}
		return sum;
	}

	string get_name() {return name;}
};

void measure_time(Integral &i, int n_threads)
{
	double result;
	time_point t1 = high_resolution_clock::now();
	if (n_threads <= 1)
		result = i.value();
	else
		result = i.value_parallel(n_threads);
	time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	cout << "Computing ∫" << i.get_name() << "(x) = " << result <<
		" using " << n_threads << " threads took " << time_span.count() << " seconds.\n";
}

int main(int argc, char **argv)
{
	auto f_sin = (double(*)(double))&sin;
	auto f_exp = (double(*)(double))&exp;
	auto f_sqr = [] (double x) {return x*x;};

	int a = 0, b = 1, N = 1000000;
	cout << "a = " << a << " b = " << b << " N = " << N << "\n" << fixed << setprecision(10);
	auto i_sin = Integral(f_sin, a, b, N, "sin");
	auto i_exp = Integral(f_exp, a, b, N, "exp");
	auto i_sqr = Integral(f_sqr, a, b, N, "sqr");

	measure_time(i_sin, 1);
	measure_time(i_exp, 1);
	measure_time(i_sqr, 1);
	measure_time(i_sin, 2);
	measure_time(i_exp, 2);
	measure_time(i_sqr, 2);
	measure_time(i_sin, 4);
	measure_time(i_exp, 4);
	measure_time(i_sqr, 4);
}
